

import java.util.Arrays;
import java.util.Scanner;
public class sort_number_algo {
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in); 
        int [] list_input = new int[10];
        
        for (int i = 0; i < 10; i++) {  //รับตัวเลขเข้า 10 ตัว
            list_input[i] = kb.nextInt();// รับตัวเลข เเละ เพิ่มตัวเลขที่รับมาใส่ใน list_input
        } 
        System.out.print("Input Number : ");
        for (int i : list_input) {

            System.out.print(i+" ");
        }// ปริ้น ค่าใน list_input มาดู
        System.out.println();
        System.out.print("Sort Number : ");
        
      
 
        for (int i = 1; i <= list_input.length-1; i++) { // รอบที่ i

            for (int j = 0; j <= list_input.length-2; j++) { //รอบที่ j
                
                if(list_input[j] > list_input[j+1]){ // sort ตัวเลขน้อยไปมาก
                    int temp = list_input[j]; // นำค่าตัวที่ j ในในตัวเเปร temp
                    list_input[j] = list_input[j+1]; //สลับ ตัวที่ list_input[j+1] ---> list_input[j]
                    list_input[j+1]= temp; //นำค่าที่เก็บใน  temp มาใส่ใน  list_input[j+1]
                }
            }
            
            System.out.print(list_input[i]+" "); // ปริ้นตัวเลขที่ sort

        }

}

}